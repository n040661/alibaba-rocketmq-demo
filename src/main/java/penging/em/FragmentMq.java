package penging.em;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 消费群组的注解
 * （用于系统启动时根据该注解去启动消费组）
 * Created by dell on 2015/12/10.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface FragmentMq {

    String nameMq() default "";

    GroupNameEm groupNameEm();
}
