package penging.em;

/**
 * 应用名称枚举
 * Created by dell on 2015/12/9.
 */
public enum GroupNameEm {

    GROUP_ALL("PENGING_GROUP"),//默认消息名称
    GROUP_1("PENGING_GROUP_1"),
    GROUP_2("PENGING_GROUP_2");

    private String name;

    GroupNameEm(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
