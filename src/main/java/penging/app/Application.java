package penging.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * mq
 * Created by zjp on 2015/12/7.
 */
@EnableAutoConfiguration
@ComponentScan(basePackages="com.penging")
@SpringBootApplication
public class Application {

    private Application(){

    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }



}
