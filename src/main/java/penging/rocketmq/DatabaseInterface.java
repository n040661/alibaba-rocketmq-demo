package penging.rocketmq;

/**
 * 这个接口用来放置参数
 * Created by dell on 2015/12/24.
 */
public interface DatabaseInterface {


    /**
     * mq请求地址
     */
    public static final String SYSTEM_NAMESRV_ADDR = "namesrv.addr";

    /**
     * mq扫描包的路径
     */
    public static final String SYSTEM_PACKAGE_URL = "scan.base.package";

}
